using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErrorShutdown : MonoBehaviour
{
    private float timer = 3f;
    public Animator animator;
    public AudioSource shutDown;


    void Update()
    {
        timer -=Time.deltaTime;

        if(timer <=0&& Input.anyKeyDown)
        {
            shutDown.Play();
           animator.SetTrigger("Trigger");
            this.gameObject.SetActive(false);
        }
    }
}
