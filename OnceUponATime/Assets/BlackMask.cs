using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BlackMask : MonoBehaviour
{
    public void GameOver(AnimationEvent e)
    {
        SceneManager.LoadScene("Meadow");
    }
}
