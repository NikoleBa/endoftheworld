using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlitchTrigger : MonoBehaviour
{

    private float timer;
    public AudioSource glitchsound;
    public float resetTimer;
    private GlitchEffect effect;
    public Camera maincamera;

    [SerializeField]
    private bool glitch = false;

    private void Awake()
    {
        timer = resetTimer;
        effect = maincamera.GetComponent<GlitchEffect>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            glitchsound.Play();
            effect.enabled = true;
            glitch = true;

        }

    }

    // Update is called once per frame
    void Update()
    {
        if (glitch)
        {
            timer -= Time.deltaTime;

            if(timer <=0)
            {
                glitch = false;
                effect.enabled = false;
                Debug.Log("Glitchoff" + this.gameObject);
                timer = resetTimer;
                Destroy(this.gameObject);
            }
        }
    }
}
