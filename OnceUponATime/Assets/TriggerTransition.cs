using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class TriggerTransition : MonoBehaviour
{
    public void LoadMeadow(AnimationEvent e)
    {
        SceneManager.LoadScene("Meadow");
    }
}
