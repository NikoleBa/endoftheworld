using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysGlitch : MonoBehaviour
{
    public GameObject enable;
    private GlitchEffect effect;
    public Camera maincamera;
    public AudioSource glitchsound;


    private void Awake()
    {
        effect = maincamera.GetComponent<GlitchEffect>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            glitchsound.Play();
             effect.enabled = true;
                enable.SetActive(true);
            Destroy(this.gameObject);

        }



    }
}