using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkSound : MonoBehaviour
{
    public AudioSource walk;
    public AudioSource run;
    public FirstPersonController controller;
    private bool isWalking = false;
    private bool isRunning = false;

    void Update()
    {
        bool walkInput = Input.GetButton("Horizontal") || Input.GetButton("Vertical");
        //bool runInput = Input.GetKey(KeyCode.LeftShift) && walkInput;
       if (walkInput && !isWalking)
        {
            //Debug.Log("Walking");
            isRunning = false;
            run.Stop();
            walk.Play();
            isWalking = true;

        }
        else if (!walkInput && isWalking)
        {
            //Debug.Log("No Walk");
            isWalking = false;
            walk.Stop();
        }
    }
}
