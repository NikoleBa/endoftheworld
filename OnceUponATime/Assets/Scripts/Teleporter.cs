using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{

    public Transform teleportToPosition;



    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            other.gameObject.transform.position = teleportToPosition.position;
            GetComponent<BoxCollider>().enabled = false;
        }
    }
}
