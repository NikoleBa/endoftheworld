using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTimer : MonoBehaviour
{
    public float timer = 15f;
    private bool started = false;

    private void Update()
    {
        if(!started)
        {
            return;
        }

        timer -= Time.deltaTime;
        if(timer<=0)
        {
            SceneManager.LoadScene("Shutdown");
        }

   
    }

    private void Start()
    {
        started = true;

    }
}
