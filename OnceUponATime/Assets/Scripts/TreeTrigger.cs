using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeTrigger : MonoBehaviour
{
    public GameObject tree;

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            tree.GetComponent<Animator>().SetTrigger("Enter");
        }
    }
}
