using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glitch_Object : MonoBehaviour
{
    public GameObject birch;
    public GameObject enable;
    public string triggername;

    public void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {

            if (triggername != null && triggername.Length > 0)
            { 
            birch.GetComponent<Animator>().SetTrigger(triggername);
            }

                if(enable!=null)
                {
                enable.SetActive(true);
                }
            }

            

     }

}
