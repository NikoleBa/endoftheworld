using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewObjects : MonoBehaviour
{
    public GameObject enableStuff;

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            enableStuff.SetActive(true);
        }
    }
}

